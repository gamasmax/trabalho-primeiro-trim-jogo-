/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhotrim1;

/**
 *
 * @author mathe
 */
public class Cavalaria extends Soldados{
    private int vida;
    private int ataque;
    public int quant;

    public Cavalaria() {
        this.vida = 80;
        this.ataque = 0;
        this.quant = 1;
    }
    
    //getters e setters
    
    public int getAtaque() {
        return ataque;
    }
 public void setAtaque(int ataque){
     this.ataque=ataque;
 }
    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }
    public int getQuant() {
        return quant;
    }

    public void setQuant(int quant) {
        this.quant = quant;
    }
    
    //metodos
    public int getFullVida()
    {
        return getVida()*getQuant();
    }
    public int getFullAtk()
    {
        return getAtaque()*getQuant();
    }
    
    //Overrides (do Abstract)
    @Override
    public int especial(){
    //FURIA: 20% mais dano
        int dano = this.getAtaque();
        dano = (int) (dano +(dano * 0.20));
        return dano;
    }
    @Override
    public int ataca() {
        return getQuant()*getAtaque();
    }

    @Override
    public void tomarDano(int dano) {
        this.vida -= dano;
    }
}
