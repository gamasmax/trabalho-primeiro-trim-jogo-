/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

;
import static control.TrabalhoTrim1.listaA;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import trabalhotrim1.Armas;
import trabalhotrim1.Artilharia;
import trabalhotrim1.Infantaria;

/**
 * FXML Controller class
 *
 * @author mathe
 */
public class FXMLModArtilhariaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    Artilharia arti = new Artilharia();
    @FXML
    private Label quantAtual;
    @FXML
    private TextField quant;
    @FXML
    private TextField atk;
    @FXML
    private Label atkAtual;
  
    @FXML
    private void voltar(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLTelaInicial.fxml");
    }
     @FXML
    private void quantAply(ActionEvent event)
    {
        arti = (Artilharia) listaA.get(0);
        arti.setQuant(Integer.parseInt(quant.getText()));
        quantAtual.setText(arti.getQuant()+" Unidades");
        listaA.add(0,arti); 
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        arti = (Artilharia) listaA.get(0);
        quantAtual.setText(arti.getQuant()+" Unidades");
        atkAtual.setText(Integer.toString(arti.getAtaque()));
        
    }    
    
}
