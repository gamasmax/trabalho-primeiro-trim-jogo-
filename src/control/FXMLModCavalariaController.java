/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;


import static control.TrabalhoTrim1.listaC;
import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import trabalhotrim1.Armas;
import trabalhotrim1.Cavalaria;


/**
 * FXML Controller class
 *
 * @author keth
 */
public class FXMLModCavalariaController implements Initializable {

 Cavalaria cav = new Cavalaria();
    Iterator i = listaC.iterator();
    /**
     * Initializes the controller class.
     */
    @FXML
    private Label quantAtual;
    @FXML
    private TextField quant;
    @FXML
    private Label atkAtual;
   
  
    @FXML
    private void voltar(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLTelaInicial.fxml");
    }
    @FXML
    private void mosqueteSet(ActionEvent event)
    {
        cav = (Cavalaria) listaC.get(0);
        cav.setAtaque(Armas.mosquete);
        atkAtual.setText("mosquete ("+Armas.mosquete+" de dano)");
        listaC.add(0,cav); 
    }
    @FXML
    private void revolverSet(ActionEvent event)
    {
        cav = (Cavalaria) listaC.get(0);
        cav.setAtaque(Armas.pistola);
        atkAtual.setText("revolver ("+Armas.pistola+" de dano)");
        listaC.add(0,cav);  
    }
    @FXML
    private void facaSet(ActionEvent event)
    {
        cav = (Cavalaria) listaC.get(0);
        cav.setAtaque(Armas.faca);
        atkAtual.setText("Faca ("+Armas.faca+" de dano)");
        listaC.add(0,cav); 
    }
    @FXML
    private void espadaSet(ActionEvent event)
    {
        cav = (Cavalaria) listaC.get(1);
        cav.setAtaque(Armas.espada);
        atkAtual.setText("Espada ("+Armas.espada+" de dano)");
        listaC.add(0,cav);  
    }
    @FXML
    private void quantAply(ActionEvent event)
    {
        cav = (Cavalaria) listaC.get(0);
        cav.setQuant(Integer.parseInt(quant.getText()));
        quantAtual.setText(cav.getQuant()+" Unidades");
        listaC.add(0,cav); 
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cav = (Cavalaria) listaC.get(0);
        quantAtual.setText(cav.getQuant()+" Unidades");
        atkAtual.setText(Integer.toString(cav.getAtaque()));
    }   
    
}
