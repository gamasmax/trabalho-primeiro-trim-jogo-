
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import static control.TrabalhoTrim1.listaI;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import trabalhotrim1.Armas;
import trabalhotrim1.Infantaria;

/**
 * FXML Controller class
 *
 * @author mathe
 */
public class FXMLModInfantariaController implements Initializable {

    Infantaria inf = new Infantaria();
    /**
     * Initializes the controller class.
     */

    @FXML
    private Label quantAtual;
    @FXML
    private TextField quant;
    @FXML
    private Label atkAtual;
    
    @FXML
    private void voltar(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLTelaInicial.fxml");
    }
    @FXML
    private void mosqueteSet(ActionEvent event)
    {
        inf = (Infantaria) listaI.get(0);
        inf.setAtaque(Armas.mosquete);
        atkAtual.setText("mosquete ("+Armas.mosquete+" de dano)");
        listaI.add(0,inf);
        System.out.println("atk infantaria " + listaI.get(0).getAtaque());
    }
    @FXML
    private void revolverSet(ActionEvent event)
    {
        inf = (Infantaria) listaI.get(0);
        inf.setAtaque(Armas.pistola);
        atkAtual.setText("revolver ("+Armas.pistola+" de dano)");
        listaI.add(0,inf);   
        System.out.println("atk infantaria " + listaI.get(0).getAtaque());
    }
    @FXML
    private void facaSet(ActionEvent event)
    {
        inf = (Infantaria) listaI.get(0);
        inf.setAtaque(Armas.faca);
        atkAtual.setText("Faca ("+Armas.faca+" de dano)");
        listaI.add(0,inf);   
        System.out.println("atk infantaria " + listaI.get(0).getAtaque());
    }
    @FXML
    private void espadaSet(ActionEvent event)
    {
        inf = (Infantaria) listaI.get(0);
        inf.setAtaque(Armas.espada);
        atkAtual.setText("Espada ("+Armas.espada+" de dano)");
        listaI.add(0,inf);  
        System.out.println("atk infantaria " + listaI.get(0).getAtaque());
    }
    @FXML
    private void quantAply(ActionEvent event)
    {
        inf = (Infantaria) listaI.get(0);
        inf.setQuant(Integer.parseInt(quant.getText()));
        quantAtual.setText(inf.getQuant() + " Unidades");
        listaI.add(0,inf); 
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inf = (Infantaria) listaI.get(0);
        quantAtual.setText(inf.getQuant() + " Unidades");
        atkAtual.setText(Integer.toString(inf.getAtaque()));  
    }   
    
}