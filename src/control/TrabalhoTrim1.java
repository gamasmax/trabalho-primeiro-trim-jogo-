/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import trabalhotrim1.Artilharia;
import trabalhotrim1.Cavalaria;
import trabalhotrim1.Infantaria;
import trabalhotrim1.Medico;
import trabalhotrim1.Soldados;

/**
 *
 * @author mathe
 */
public class TrabalhoTrim1 extends Application {
    private static Stage stage;
    
    public static ArrayList<Infantaria> listaI = new ArrayList<>();
    public static ArrayList<Medico> listaM = new ArrayList<>();
    public static ArrayList<Cavalaria> listaC = new ArrayList<>();
    public static ArrayList<Artilharia> listaA = new ArrayList<>();
    public static ArrayList<ArrayList> listaL= new ArrayList<>();
    public static int cont = 0;
    
    public static Stage getStage() {
        return stage;
    }
    
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(TrabalhoTrim1.class.getResource(tela));
        }
        catch(Exception e){
            System.out.println("Verificar arquivo FXML");
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        
        stage.setScene(scene); 
        stage.show();
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLInicial.fxml"));
        
        Scene scene = new Scene(root);
        //observar essa linha = 
        TrabalhoTrim1.stage = stage;
        
        stage.setScene(scene);
        stage.show();
        trocaTela("FXMLInicial.fxml");// é só criar uma string com o nome da tela que é para ir
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        listaL.add(listaI);
        listaL.add(listaC);
        listaL.add(listaM);
        listaL.add(listaA);
        launch(args);
    }
    
}
