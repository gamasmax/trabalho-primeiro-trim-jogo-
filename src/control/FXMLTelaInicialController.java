/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import static control.TrabalhoTrim1.cont;
import static control.TrabalhoTrim1.listaA;
import static control.TrabalhoTrim1.listaC;
import static control.TrabalhoTrim1.listaI;
import static control.TrabalhoTrim1.listaM;
import trabalhotrim1.Infantaria;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import trabalhotrim1.Artilharia;
import trabalhotrim1.Cavalaria;
import trabalhotrim1.IA;
import trabalhotrim1.Medico;
import trabalhotrim1.Usuario;

/**
 * FXML Controller class
 *
 * @author mathe
 */
public class FXMLTelaInicialController implements Initializable {

    @FXML
    private Label infantariahp, infantariaatk, quantInf;
    
    @FXML
    private Label cavalariahp, cavalariaatk, quantCav;
    
    @FXML
    private Label medicohp, medicoatk, quantMed;
    
    @FXML
    private Label artilhariahp, artilhariaatk, quantArti;
    @FXML
    private Label vida_total_exercito, atk_total_exercito;
    
    @FXML
    private void batalhar(ActionEvent event) 
    {
        TrabalhoTrim1.trocaTela("FXMLBatalha.fxml");
    }
    @FXML
    private void modficaArti(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLModArtilharia.fxml");
    }
    @FXML
    private void modficaMed(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLModMedico.fxml");
    }
    @FXML
    private void modficaInf(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLModInfantaria.fxml");
    }
    @FXML
    private void modficaCav(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLModCavalaria.fxml");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        IA ia = new IA();
        ia.geradorDeVidaAtaque();
        Infantaria i1 = new Infantaria();
        Cavalaria c1 = new Cavalaria();
        Medico m1 = new Medico();
        Artilharia a1 = new Artilharia();
        int ataqueTotal=0, vidaTotal=0;
        
        
        if(cont<=0)
        {
            listaI.add(i1);
            listaC.add(c1);
            listaM.add(m1);
            listaA.add(a1);
            ataqueTotal = listaI.get(0).getFullAtk() + listaC.get(0).getFullAtk() + listaA.get(0).getFullAtk();
            vidaTotal = listaI.get(0).getFullVida() + listaC.get(0).getFullVida() + listaA.get(0).getFullVida() + listaM.get(0).getFullVida();
            infantariahp.setText(Integer.toString(listaI.get(0).getVida()));
            quantInf.setText(Integer.toString(listaI.get(0).getQuant()));
            infantariaatk.setText(Integer.toString(listaI.get(0).getAtaque()));
            cavalariahp.setText(Integer.toString(listaC.get(0).getVida()));
            quantCav.setText(Integer.toString(listaC.get(0).getQuant()));
            cavalariaatk.setText(Integer.toString(listaC.get(0).getAtaque()));
            medicohp.setText(Integer.toString(listaM.get(0).getVida()));
            quantMed.setText(Integer.toString(listaM.get(0).getQuant()));
            medicoatk.setText("Medico não contem ataque padrão");
            artilhariahp.setText(Integer.toString(listaA.get(0).getVida()));
            artilhariaatk.setText(Integer.toString(listaA.get(0).getAtaque()));
            quantArti.setText(Integer.toString(listaA.get(0).getQuant()));
            atk_total_exercito.setText(Integer.toString(ataqueTotal));
            vida_total_exercito.setText(Integer.toString(vidaTotal));
            cont++;
        }
        else
        {
            ataqueTotal = listaI.get(0).getFullAtk() + listaC.get(0).getFullAtk() + listaA.get(0).getFullAtk();
            vidaTotal = listaI.get(0).getFullVida() + listaC.get(0).getFullVida() + listaA.get(0).getFullVida() + listaM.get(0).getFullVida();
            infantariahp.setText(Integer.toString(listaI.get(0).getVida()));
            quantInf.setText(Integer.toString(listaI.get(0).getQuant()));
            infantariaatk.setText(Integer.toString(listaI.get(0).getAtaque()));
            cavalariahp.setText(Integer.toString(listaC.get(0).getVida()));
            quantCav.setText(Integer.toString(listaC.get(0).getQuant()));
            cavalariaatk.setText(Integer.toString(listaC.get(0).getAtaque()));
            medicohp.setText(Integer.toString(listaM.get(0).getVida()));
            quantMed.setText(Integer.toString(listaM.get(0).getQuant()));
            medicoatk.setText("Medico não contem ataque padrão");
            artilhariahp.setText(Integer.toString(listaA.get(0).getVida()));
            artilhariaatk.setText(Integer.toString(listaA.get(0).getAtaque()));
            quantArti.setText(Integer.toString(listaA.get(0).getQuant()));
        
            atk_total_exercito.setText(Integer.toString(ataqueTotal));
            vida_total_exercito.setText(Integer.toString(vidaTotal));
            
            Usuario u = new Usuario();
           
            u.setVidaTotal(vidaTotal);
            System.out.println(u.getVidaTotal());
            u.setAtaqueTotal(ataqueTotal);
            System.out.println(u.getAtaqueTotal());
            System.out.println("atk infantaria " + listaI.get(0).getAtaque());
        }
    }
}
    
