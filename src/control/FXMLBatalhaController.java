/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import trabalhotrim1.IA;
import trabalhotrim1.Soldados;
import trabalhotrim1.Usuario;


/**
 *
 * @author keth
 */
public class FXMLBatalhaController implements Initializable {
    @FXML
    private Label quantVidaUser; 
    @FXML 
    private Label quantAtaqueUser;
    @FXML 
    private Label quantAtaqueIA;
    @FXML 
    private Label quantVidaIA;
    @FXML
    private Label mostraVencedor;
    @FXML
    private Label turnosVencidosUser;
    @FXML
    private Label turnosVencidosIA;
    private int contIa=0;
    private int contUser=0;
    @FXML Button batalhar;
    @FXML
    private void voltar(ActionEvent event)
    {
        TrabalhoTrim1.trocaTela("FXMLTelaInicial.fxml");
    }
    
    @FXML 
    private void batalhar(ActionEvent event){
        Usuario u = new Usuario();
       
        IA ia= new IA();
    while(u.getVidaTotal()>0 && ia.getVidaTotal()>0){
    quantVidaUser.setText(Integer.toString(u.getVidaTotal()));
    quantAtaqueUser.setText(Integer.toString(u.getAtaqueTotal()));
    quantVidaIA.setText(Integer.toString(ia.getVidaTotal()));
    quantAtaqueIA.setText(Integer.toString(ia.getAtaqueTotal()));
           if(u.getVidaTotal() < ia.getVidaTotal()){
               turnosVencidosIA.setText(Integer.toString(contIa + 1));
                mostraVencedor.setText("IA venceu esta partida");
           }else{
                turnosVencidosUser.setText(Integer.toString(contUser + 1));
                mostraVencedor.setText("User venceu esta partida");
               
           }if(u.getVidaTotal()==ia.getVidaTotal()){
               turnosVencidosIA.setText(Integer.toString(contIa + 1));
               turnosVencidosUser.setText(Integer.toString(contUser + 1));
               mostraVencedor.setText("empate");
           }
           } 
    
    if(contIa<contUser){
        mostraVencedor.setText("usuário venceu");
    }if(contUser<contIa){
        mostraVencedor.setText("IA venceu, que vergonha D:");
    }if(contIa==contUser){
        mostraVencedor.setText("os dois venceram!");
    }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ArrayList<Object> Lista = new ArrayList();
    }    
    
}
